import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:squrex/Screens/ContactUs.dart';
import 'package:squrex/Screens/HomePage.dart';
import 'package:squrex/Screens/LoginPage.dart';
import 'package:squrex/Screens/MyOrders.dart';
import 'package:squrex/Screens/NotifcationsPage.dart';
import 'package:squrex/Screens/PhotoGallery.dart';
import 'package:squrex/Utils/Colors.dart';
import 'package:squrex/Utils/TextStyles.dart';


class CustomDrawer extends StatelessWidget {
  final int index;
  CustomDrawer({this.index});
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        width: Get.width*0.65,
        child: Drawer(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  margin: EdgeInsets.only(left: 30, top: 30),
                  height: 80,
                  width: 80,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [Color(0xffba68c8),Color(0xff7e57c2)],
                        begin: AlignmentDirectional.topStart,
                        end: AlignmentDirectional.bottomEnd
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(40)),
                    border: Border.all(color: greyShade, width: 0.5),
                  ),
                  child: Icon(
                    FontAwesomeIcons.userAlt,
                    color: Color(0xffd1c4e9),
                    size: 40,
                  )
              ),
              Container(
                margin: EdgeInsets.only(left: 30, top: 15),
                child: Text(
                  "+1 202 324 234",
                  style: TextStyle(
                      color: black,
                      fontSize: 16,
                      fontWeight: FontWeight.w700
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              GestureDetector(
                onTap: (){
                  if(index==0){
                    Get.back();
                  }
                  else{
                    Get.off(HomePage());
                  }
                },
                child: DrawerItem(
                  drawerName: "Home",
                  notification: 0,
                ),
              ),
              GestureDetector(
                onTap: (){
                  if(index==1){
                    Get.back();
                  }
                  else{
                    Get.off(MyOrders());
                  }
                },
                child: DrawerItem(
                  drawerName: "My Orders",
                  notification: 0,
                ),
              ),
              GestureDetector(
                onTap: (){
                  if(index==2){
                    Get.back();
                  }
                  else{;
                    Get.off(NotificationsPage());
                  }
                },
                child: DrawerItem(
                  drawerName: "Notifications",
                  notification: 2,
                ),
              ),
              GestureDetector(
                onTap: (){
                  if(index==3){
                    Get.back();
                  }
                  else{
                    Get.off(PhotoGallery());
                  }
                },
                child: DrawerItem(
                  drawerName: "Photos",
                  notification: 0,
                ),
              ),
              GestureDetector(
                onTap: (){
                  if(index==4){
                    Get.back();
                  }
                  else{
                    Get.off(ContactUs());
                  }
                },
                child: DrawerItem(
                  drawerName: "Contact Us",
                  notification: 0,
                ),
              ),
              GestureDetector(
                onTap: (){
                  Get.back();
                  Get.dialog(
                      Dialog(
                        insetPadding: EdgeInsets.symmetric(horizontal: 5),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(20))
                        ),
                        child: Container(
                          height: MediaQuery.of(context).size.height*0.2,
                          width: MediaQuery.of(context).size.width*0.7,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                height: 15,
                              ),
                              Text(
                                "Are you sure you want to logout?",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    GestureDetector(
                                      onTap:(){
                                        Get.offAll(LoginPage());
                                      },
                                      child: Text(
                                        "Yes",
                                        style: logOutOptionStyle
                                      ),
                                    ),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    GestureDetector(
                                      onTap:(){
                                        Get.back();
                                      },
                                      child: Text(
                                        "No",
                                        style: logOutOptionStyle,
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                  );
                },
                child: DrawerItem(
                  drawerName: "Log out",
                  notification: 0,
                ),
              ),
              SizedBox(
                width: Get.width*0.65,
                child: Divider(
                  color: Colors.grey[300],
                  thickness: 1,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


class DrawerItem extends StatelessWidget {
  final String drawerName;
  final int notification;
  DrawerItem({this.drawerName, this.notification});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          SizedBox(
            width: Get.width*0.65,
            child: Divider(
              color: Colors.grey[300],
              thickness: 1,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 25, right: 20, top: 8, bottom: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  drawerName,
                  style: TextStyle(
                      color: black,
                      fontSize: 14,
                      fontWeight: FontWeight.w500
                  ),
                ),
                SizedBox(
                  height: 20,
                  width: 20,
                  child: Visibility(
                    visible: notification!=0,
                    child: CircleAvatar(
                      radius: 20,
                      backgroundColor: Color(0xffffebee),
                      child: Text(
                        notification.toString(),
                        style: TextStyle(
                            color: red,
                            fontSize: 12,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}