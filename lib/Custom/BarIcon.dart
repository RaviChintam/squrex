import 'package:flutter/material.dart';

class BarIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 1.7,
          width: 20,
          color: Colors.black,
        ),
        SizedBox(
          height: 3,
        ),
        Container(
          height: 2.4,
          width: 20,
          color: Colors.black,
        ),

        SizedBox(
          height: 3,
        ),
        Container(
          height: 3.1,
          width: 20,
          color: Colors.black,
        ),

      ],
    );
  }
}
