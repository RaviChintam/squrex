import 'package:flutter/material.dart';
import 'package:squrex/Utils/Colors.dart';

class CustomButton extends StatelessWidget {
  final String name;
  CustomButton({this.name});
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        width: 320,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: iconBackground,
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd
            ),
            borderRadius: BorderRadius.all(Radius.circular(15)),
            border: Border.all(color: greyShade, width: 0.5),
            boxShadow: [
              BoxShadow(
                  color: Color(0xff7e57c2).withOpacity(0.6),
                  offset: Offset(0.0,3.0),
                  blurRadius: 10,
                  spreadRadius: 1
              ),
              BoxShadow(
                  color: white,
                  offset: Offset(-3.0,-3.0),
                  blurRadius: 15.0,
                  spreadRadius: 1.0
              ),
            ]
        ),
        child: Center(
          child: Text(
            name,
            style: TextStyle(
                color: white,
                fontSize: 18,
                fontWeight: FontWeight.w500
            ),
          ),
        )
    );
  }
}
