import 'package:flutter/material.dart';
import 'package:squrex/Utils/Colors.dart';


class TextFieldOutline extends StatelessWidget {
  final Widget child;
  TextFieldOutline({this.child});
  @override
  Widget build(BuildContext context) {
    return Container(

      height: 50,
      width: 320,
      decoration: BoxDecoration(
          color: white,
          borderRadius: BorderRadius.all(Radius.circular(15)),
          border: Border.all(color: greyShade, width: 0.5),
          boxShadow: [
            BoxShadow(
                color: greyShade,
                offset: Offset(3.0,3.0),
                blurRadius: 15.0,
                spreadRadius: 1.0
            ),
            BoxShadow(
                color: white,
                offset: Offset(-3.0,-3.0),
                blurRadius: 15.0,
                spreadRadius: 1.0
            ),
          ]
      ),
      child: child,
    );
  }
}
