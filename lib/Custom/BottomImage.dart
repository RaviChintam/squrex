import 'package:flutter/material.dart';

class BottomImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height*0.3,
      width: MediaQuery.of(context).size.width,
      child: Image(
        image: AssetImage(
          "Images/LoginImage.png",
        ),
        fit: BoxFit.fill,
      ),
    );
  }
}
