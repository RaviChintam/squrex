import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:squrex/Utils/Colors.dart';



class DashedContainer extends StatelessWidget {
  final String name;
  final bool center;
  final double height,width;
  DashedContainer({this.name, this.center, this.width = 320,this.height = 50});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      height:height,
      width: width,
      child: DottedBorder(
        color: grey,
        padding: EdgeInsets.symmetric(horizontal: 15),
        strokeCap: StrokeCap.butt,
        dashPattern: [10,8],
        borderType: BorderType.RRect,
        radius: Radius.circular(15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: center?CrossAxisAlignment.center:CrossAxisAlignment.start,
          children: [
            Text(
              name,
              style: TextStyle(
                  color: Colors.grey[500],
                  fontSize: 14,
                  fontWeight: FontWeight.w400
              ),
            ),
          ],
        ),
      ),
    );
  }
}