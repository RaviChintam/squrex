import 'package:flutter/material.dart';
import 'package:squrex/Utils/Colors.dart';

final inputFieldStyle = InputDecoration(
    border: InputBorder.none,
    hintText: "Enter you mobile number",
    hintStyle: TextStyle(
        color: Colors.grey[400],
        fontSize: 16,
        fontWeight: FontWeight.w400
    ),
    contentPadding: EdgeInsets.symmetric(horizontal: 15)
);



final InputDecoration fieldDecor = InputDecoration(      // Request Page
    border: InputBorder.none,
    hintStyle: TextStyle(
        color: Colors.grey[500],
        fontSize: 14,
        fontWeight: FontWeight.w400
    ),
    contentPadding: EdgeInsets.symmetric(horizontal: 15)
);


final InputDecoration contactFieldDecor = InputDecoration(
    border: InputBorder.none,
    hintStyle: TextStyle(
        color: grey,
        fontSize: 14,
        fontWeight: FontWeight.w500
    ),
    contentPadding: EdgeInsets.symmetric(horizontal: 15)
);
