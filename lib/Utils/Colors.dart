import 'package:flutter/material.dart';

Color blue = Colors.blue;
Color white = Colors.white;
Color greyBlack = Color(0x5f000000); // Back Button color
Color black = Colors.black;
Color grey = Colors.grey;
Color greyShade = Colors.grey[400];
List<Color> gradient = [Color(0xff05A4FF), Color(0xff42a5f5)];
List<Color> iconBackground = [Color(0xffa765ff),Color(0xff8c66ff)];
Color purple = Color(0xffa765ff);
Color red = Colors.red;