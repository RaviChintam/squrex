
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:squrex/Utils/Colors.dart';

final BoxDecoration fieldBoxDecor = BoxDecoration(
    color: white,
    borderRadius: BorderRadius.all(Radius.circular(15)),
    boxShadow: [
      BoxShadow(
          color: greyShade,
          offset: Offset(3.0,3.0),
          blurRadius: 15.0,
          spreadRadius: 1.0
      ),
      BoxShadow(
          color: white,
          offset: Offset(-3.0,-3.0),
          blurRadius: 15.0,
          spreadRadius: 1.0
      ),
    ]
);