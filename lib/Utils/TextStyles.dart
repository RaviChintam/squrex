import 'package:flutter/cupertino.dart';
import 'package:squrex/Utils/Colors.dart';



//Splash Page
final splashHeaderStyle = TextStyle(
    color: white,
    fontSize: 50,
    fontWeight: FontWeight.w400,
    letterSpacing: 1.5
);


//Login Page
final loginHeaderStyle = TextStyle(             //used in Verification Page too
    color: Color(0xff7c4dff),
    fontFamily: "AristaPro",
    fontSize: 45,
    letterSpacing: 1,
);

final welcomeStyle = TextStyle(
    color: black,
    fontSize: 25,
    fontWeight: FontWeight.w500,
    letterSpacing: 1
);

final loginMessageStyle = TextStyle(
    color: grey,
    fontSize: 18,
    fontWeight: FontWeight.w300,
    letterSpacing: 1
);

final loginHintStyle = TextStyle(
    color: grey,
    fontWeight: FontWeight.w400
);


//VerificationPage

final verifyHeaderStyle = TextStyle(
    color: black,
    fontSize: 25,
    fontWeight: FontWeight.w500,
    letterSpacing: 1
);

final verifyMessageStyle = TextStyle(
    color: grey,
    fontSize: 14,
    fontWeight: FontWeight.w300,
    letterSpacing: 1
);



final appBarStyle = TextStyle(
    color: black,
    fontSize: 16,
    fontWeight: FontWeight.w500
);


// Request Page

final selectTypeStyle = TextStyle(
    color: black,
    fontSize: 16,
    fontWeight: FontWeight.w500
);

final requestHeaderStyle = TextStyle(
  color: black,
  fontSize: 20,
  height: 1.4,
  fontWeight: FontWeight.w600,
);

final requestMessageStyle = TextStyle(
  color: black,
  fontSize: 15,
  height: 1.3,
  fontWeight: FontWeight.w300,
);

final sideHeaderStyle = TextStyle(
    color: grey,
    fontSize: 15,
    fontWeight: FontWeight.w500
);

final valueStyle = TextStyle(
    color: black,
    fontSize: 18,
    fontWeight: FontWeight.w700
);


//My Orders Page

final orderIdStyle = TextStyle(
    color: black,
    fontSize: 16,
    fontWeight: FontWeight.w600
);

final orderDateStyle = TextStyle(
    color: grey,
    fontSize: 12,
    fontWeight: FontWeight.w400
);

final catQuesStyle = TextStyle(
    color: grey,
    fontSize: 12,
    fontWeight: FontWeight.w400
);

final catAnsStyle = TextStyle(
    color: black,
    fontSize: 14,
    fontWeight: FontWeight.w400
);


//logout styles

final logOutOptionStyle = TextStyle(
    color: purple,
    fontSize: 16,
    fontWeight: FontWeight.w600
);