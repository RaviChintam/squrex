
final con = "Continue";

// Splash Page and Login Page

final appName = "SqureX";

// Login Page

final welcome = "Welcome to SqureX,";
final loginMessage = "Log into your account";
final loginFieldHint = "Enter you mobile number";
final loginHint = "We will send you 4 digit verification code";


// VerificationPage

final verifyHeader = "Verification";
final verifyMessage = "We just sent you a 4 digit verification code at +97 8987 98437 please enter below";
final changeNumber = "Change mobile number";
final resend = "Resend Code";


//Home Page

final homeAppBar = "Home";
final requestAppBar = "Request new electricity order";


// Request Page

final selectType = "Please select type";

final requestHeader = "Request New Electricity Order For Shop";
final requestMessage = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse sem augue, varius ac dolor in, tristique lobortis enim. Ut at hendrerit eros. ";

final nameField = "Write your full name";
final photoField = "Upload Civil ID Photo";
final certifyField = "Upload Shop Certificate";
final addressField = "Write shop full address";

final amount = "Amount:";

final requestButton = "Make Payment";


// My Orders Page

final orderAppBar = "My Orders";
final orderId = "Order id: ";
final status = "Status:";
final category = "Category:";
final subCat = "Subcategory:";


// Contact Us Page

final contactAppBar = "Contact Us";
final nameContactField = "Name";
final emailField = "Email";
final subjectField = "Subject";
final yourMessageField = "Your Message";
final contactButton = "Send";


// Photo Page

final photoAppBar = "Photo Gallery";