import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:squrex/Screens/SplashScreen.dart';

void main() {
  runApp(
      GetMaterialApp(
          home: SplashScreen(),
        debugShowCheckedModeBanner: false,
  ));
}