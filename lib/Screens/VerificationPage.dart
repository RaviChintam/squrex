import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:squrex/Custom/BottomImage.dart';
import 'package:squrex/Custom/CustomButton.dart';
import 'package:squrex/Screens/HomePage.dart';
import 'package:squrex/Screens/LoginPage.dart';
import 'package:squrex/Utils/Colors.dart';
import 'package:squrex/Utils/Strings.dart';
import 'package:squrex/Utils/TextStyles.dart';

class VerificationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              Positioned(
                bottom: 0,
                child: BottomImage(),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height*0.12,
                    ),
                    Text(
                      appName,
                      style: loginHeaderStyle,
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height*0.08,
                    ),
                    Text(
                      verifyHeader,
                      style: verifyHeaderStyle,
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height*0.015,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.1),
                      child: Text(
                        verifyMessage,
                        textAlign: TextAlign.center,
                        style: verifyMessageStyle,
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height*0.05,
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width*0.8,
                        child: PinCodeTextField(
                              textInputAction: TextInputAction.done,
                              keyboardType: TextInputType.number,
                              appContext: context,
                              length: 4,
                              textStyle: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.w600
                              ),
                              obscureText: false,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              animationType: AnimationType.fade,
                              boxShadows: [
                                BoxShadow(
                                    color: greyShade,
                                    offset: Offset(3.0,3.0),
                                    blurRadius: 15.0,
                                    spreadRadius: 1.0
                                ),
                                BoxShadow(
                                    color: white,
                                    offset: Offset(-3.0,-3.0),
                                    blurRadius: 15.0,
                                    spreadRadius: 1.0
                                ),
                              ],
                              pinTheme: PinTheme(
                                inactiveFillColor: Colors.white,
                                selectedFillColor: Colors.white,
                                shape: PinCodeFieldShape.box,
                                borderWidth: 0.2,
                                borderRadius: BorderRadius.circular(
                                  10,
                                ),
                                fieldWidth: 60,
                                fieldHeight: 60,
                                activeFillColor: Colors.white,
                                activeColor: Colors.grey,
                                selectedColor: Colors.grey,
                                inactiveColor: Colors.grey,
                                //selectedFillColor: Colors.transparent
                              ),
                              animationDuration: Duration(milliseconds: 300),
                              // backgroundColor: Colors.white,
                              enableActiveFill: true,
                              onCompleted: (value) {
          //                            setState(() {
          //                              print("FULL VALUE : " + value);
          //                              smsOTP = value;
          //                            });
                              },
                              onChanged: (value) {
                                print(value);
          //                            setState(() {
          //                              smsOTP = value;
          //                            });
                              },
                              beforeTextPaste: (text) {
                                print("Allowing to paste $text");
                                //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                                //but you can show anything you want here, like your pop up saying wrong paste format or etc
                                return true;
                              },
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height*0.04,
                    ),
                    GestureDetector(
                      onTap: (){
                        Get.to(HomePage());
                      },
                      child: CustomButton(
                        name: con,
                      )
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height*0.03,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.07),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          GestureDetector(
                            onTap: (){
                              Get.off(LoginPage(), transition: Transition.fadeIn, duration: Duration(milliseconds: 400));
                            },
                            child: Row(
                              children: [
                                Icon(
                                  Icons.keyboard_arrow_left,
                                  size: 18,
                                ),
                                Text(
                                  changeNumber,
                                   style: TextStyle(
                                     fontWeight: FontWeight.w500,
                                     fontSize: 14
                                   ),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                            width: 5,
                            child: VerticalDivider(
                              width: 2,
                              thickness: 2,
                            ),
                          ),
                          Text(
                            resend,
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 14
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
