import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:squrex/Custom/BarIcon.dart';
import 'package:squrex/Custom/BottomImage.dart';
import 'package:squrex/Custom/Drawer.dart';
import 'package:squrex/Screens/SelectType.dart';
import 'package:squrex/Utils/Colors.dart';
import 'package:squrex/Utils/Strings.dart';
import 'package:squrex/Utils/TextStyles.dart';

class HomePage extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: white,
      appBar: AppBar(
        backgroundColor: white,
        elevation: 0,
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: GestureDetector(
              onTap: (){
                _scaffoldKey.currentState.openDrawer();
              },
              child: BarIcon()
            ),
          ),
        ),
        centerTitle: true,
        title: Text(
          homeAppBar,
          style: appBarStyle,
        ),
      ),
      drawer: CustomDrawer(index: 0,),
      body: Container(
        height: Get.height,
        child: Stack(
          children: [
            Positioned(
              bottom: 0,
              child: BottomImage(),
            ),
            ListView(
              children: [
                SizedBox(
                  height: Get.height*0.05,
                ),
                CategoryCard(icon: FontAwesomeIcons.solidPaperPlane, title: "Request New Electricity Order", backImage: "https://images.unsplash.com/photo-1530018607912-eff2daa1bac4?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1351&q=80",),
                CategoryCard(icon: FontAwesomeIcons.solidPlusSquare, title: "Request New Electricity Connection", backImage: "https://images.unsplash.com/photo-1530018607912-eff2daa1bac4?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1351&q=80",),
                CategoryCard(icon: FontAwesomeIcons.arrowUp, title: "Request Electricity Upgrade", backImage: "https://images.unsplash.com/photo-1530018607912-eff2daa1bac4?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1351&q=80",),
              ],
            ),
          ],
        ),
      ),
    );
  }
}


class CategoryCard extends StatelessWidget {
  final IconData icon;
  final String title, backImage;
  CategoryCard({this.icon,this.title,this.backImage});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Get.to(SelectType());
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: Get.width*0.1, vertical: 15),
        height: 100,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10)
        ),
        child: Stack(
          children: [
            Container(
              width: Get.width*0.8,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image(
                  image: NetworkImage(backImage),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Container(
              height: 100,
              decoration: BoxDecoration(
                  color: Colors.black.withOpacity(0.6),
                  borderRadius: BorderRadius.circular(10)
              ),
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Center(
                child: Row(
                  children: [
                    Container(
                        margin: EdgeInsets.symmetric(horizontal: 15),
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                              colors: iconBackground,
                              begin: AlignmentDirectional.topStart,
                              end: AlignmentDirectional.bottomEnd
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        ),
                        child: Icon(
                          icon,
                          color: white,
                          size: 20,
                        )
                    ),
                    SizedBox(
                      width: 2,
                    ),
                    Flexible(
                      child: Text(
                        title,
                        style: TextStyle(
                            color: white,
                            fontSize: 18,
                            fontWeight: FontWeight.w500
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}



