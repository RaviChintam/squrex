import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:squrex/Custom/CustomButton.dart';
import 'package:squrex/Custom/DashedContainer.dart';
import 'package:squrex/Screens/HomePage.dart';
import 'package:squrex/Utils/Colors.dart';
import 'package:squrex/Utils/Decorations.dart';
import 'package:squrex/Utils/Strings.dart';
import 'package:squrex/Utils/TextFieldDecor.dart';
import 'package:squrex/Utils/TextStyles.dart';

class RequestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: white,
        leading: GestureDetector(
          onTap: (){
            Get.back();
          },
          child: Icon(
            Icons.arrow_back,
            color: black,
          ),
        ),
        elevation: 2,
        title: Text(
          requestAppBar,
          style: appBarStyle,
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          width: Get.width,
          margin: EdgeInsets.symmetric(horizontal: Get.width*0.1),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: Get.height*0.04,),
                child: Text(
                  requestHeader,
                  textAlign: TextAlign.center,
                  style: requestHeaderStyle
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: Get.height*0.03,),
                child: Text(
                  requestMessage,
                  textAlign: TextAlign.center,
                  style: requestMessageStyle
                ),
              ),
              Container(
                height: 50,
                width: 320,
                margin: EdgeInsets.symmetric(vertical: 10),
                decoration: fieldBoxDecor,
                child: TextField(
                  decoration: fieldDecor.copyWith(
                    hintText: nameField,
                  ),
                ),
              ),
              DashedContainer(
                name: photoField,
                center: false,
              ),
              DashedContainer(
                name: certifyField,
                center: false,
              ),
              Container(
                height: 50,
                margin: EdgeInsets.symmetric(vertical: 10),
                width: 320,
                decoration: fieldBoxDecor,
                child: TextField(
                  decoration: fieldDecor.copyWith(
                    hintText: addressField,
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                amount,
                style: sideHeaderStyle,
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                "120 USD",
                style: valueStyle,
              ),
              SizedBox(
                height: 30,
              ),
              GestureDetector(
                onTap: (){
                  Get.offAll(HomePage());
                },
                child: CustomButton(
                  name:requestButton
                ),
              ),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}



