
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:squrex/Screens/LoginPage.dart';
import 'package:squrex/Utils/Strings.dart';
import 'package:squrex/Utils/TextStyles.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(
        Duration(seconds: 3),
            (){
          Get.off(LoginPage());
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffa259ff),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              appName,
              style: splashHeaderStyle,
            )
          ],
        ),
      ),
    );
  }
}
