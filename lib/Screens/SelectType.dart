import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:squrex/Screens/RequestPage.dart';
import 'package:squrex/Utils/Colors.dart';
import 'package:squrex/Utils/Strings.dart';
import 'package:squrex/Utils/TextStyles.dart';

class SelectType extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: white,
        leading: GestureDetector(
          onTap: (){
            Get.back();
          },
          child: Icon(
            Icons.arrow_back,
            color: black,
          ),
        ),
        elevation: 2,
        title: Text(
          requestAppBar,
          style: appBarStyle,
        ),
        centerTitle: true,
      ),
      body: Container(
        width: Get.width,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(vertical: Get.height*0.04,),
              child: Text(
                selectType,
                style: selectTypeStyle
              ),
            ),
            TypeCards(),
            TypeCards(),
            TypeCards()
          ],
        ),
      ),
    );
  }
}



class TypeCards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Get.to(RequestPage());
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 20),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 13),
        width: Get.width*0.8,
        decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                  color: greyShade,
                  offset: Offset(3.0,3.0),
                  blurRadius: 15.0,
                  spreadRadius: 1.0
              ),
              BoxShadow(
                  color: white,
                  offset: Offset(-3.0,-3.0),
                  blurRadius: 15.0,
                  spreadRadius: 1.0
              ),
            ]
        ),
        child: Row(
          children: [
            Icon(
              FontAwesomeIcons.home,
              size: 35,
            ),
            SizedBox(
              width: 20,
            ),
            SizedBox(
              height: 40,
              child: VerticalDivider(
                thickness: 2,
                width: 1,
              ),
            ),
            SizedBox(
              width: 15,
            ),
            Text(
              "Home",
              style: TextStyle(
                  color: black,
                  fontSize: 16,
                  fontWeight: FontWeight.w500
              ),
            )
          ],
        ),
      ),
    );
  }
}
