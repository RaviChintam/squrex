import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:squrex/Custom/BarIcon.dart';
import 'package:squrex/Custom/Drawer.dart';
import 'package:squrex/Utils/Colors.dart';
import 'package:squrex/Utils/TextStyles.dart';

class NotificationsPage extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: white,
      appBar: AppBar(
        backgroundColor: white,
        elevation: 2,
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: GestureDetector(
              onTap: (){
                _scaffoldKey.currentState.openDrawer();
              },
              child: BarIcon(),
            ),
          ),
        ),
        centerTitle: true,
        title: Text(
          "Notifications",
          style: appBarStyle,
        ),
      ),
      drawer: CustomDrawer(index: 2,),
      body: Container(
        height: Get.height,
        child: Stack(
          children: [
            ListView(
              children: [
                SizedBox(
                  height: Get.height*0.05,
                ),
                NotificationCard(isActive: true,),
                NotificationCard(isActive: true,),
                NotificationCard(isActive: false,),
                NotificationCard(isActive: false,)
              ],
            )
          ],
        ),
      ),
    );
  }
}


class NotificationCard extends StatelessWidget {
  final bool isActive;
  NotificationCard({this.isActive});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: Get.width*0.08, vertical: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Stack(
                children: [
                  Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      border: Border.all(color: isActive?greyShade:Colors.grey[300], width: 0.8)
                    ),
                    child: Icon(
                      Icons.bolt,
                      size: 25,
                      color: isActive?Colors.amber:Colors.amber.withOpacity(0.5),
                    ),
                  ),
                  Visibility(
                    visible: !isActive,
                    child: Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                        color: Colors.white.withOpacity(0.4)
                      )
                    ),
                  ),
                ],
              ),
              Container(
                width: Get.width*0.7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text(
                        "Agent assigned to your order no. 3432432",
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: isActive?Colors.black:Colors.grey[500]
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "2min ago",
                      style: TextStyle(
                        color: isActive?grey:Colors.grey[400],
                        fontWeight: FontWeight.w400,
                        fontSize: 12
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
          SizedBox(
            height: 20,
          ),
          SizedBox(
            width: Get.width*0.9,
            child: Divider(
              color: greyShade,
              thickness: 1,
            ),
          )
        ],
      ),
    );
  }
}
