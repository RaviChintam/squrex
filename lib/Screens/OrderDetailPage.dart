import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:squrex/Custom/CustomButton.dart';
import 'package:squrex/Custom/DashedContainer.dart';
import 'package:squrex/Utils/Colors.dart';
import 'package:timelines/timelines.dart';

class OrderDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      appBar: AppBar(
        backgroundColor: white,
        elevation: 2,
        leading: GestureDetector(
          onTap: (){
            Get.back();
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Icon(
              Icons.arrow_back,
              size: 30,
              color: black,
            ),
          ),
        ),
        centerTitle: true,
        title: Text(
          "Order id: 35642362",
          style: TextStyle(
              color: black,
              fontSize: 16,
              fontWeight: FontWeight.w500
          ),
        ),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: Get.width*0.08),
        child: ListView(
          children: [
            SizedBox(
              height: Get.height*0.05,
            ),
            Text(
              "Order id: 35642362",
              textAlign: TextAlign.left,
              style: TextStyle(
                  color: black,
                  fontSize: 16,
                  fontWeight: FontWeight.w600
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "21 Jan 2020 2:00am",
              textAlign: TextAlign.left,
              style: TextStyle(
                  color: grey,
                  fontSize: 12,
                  fontWeight: FontWeight.w400
              ),
            ),
            ShowDetails(cat: "Request for", ans: "Shop",),
            ShowDetails(cat: "Address", ans: "3517 W. Gray St. Utica, Pennsylvania 57867",),
            ShowDetails(cat: "Your name", ans: "Jack Lacross",),
            ShowDetails(cat: "Agent", ans: "Torrent Power",),
            ShowDetails(cat: "Mobile number", ans: "+1 202 243 432",),
            ShowDetails(cat: "Payment status", ans: "Success",),
            SizedBox(
              height: 20,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                timelineRow("Order Created", "21 Jan 2020 2:00am", true, true,true),
                timelineRow("Agent xxx updated status abc", "21 Jan 2020 2:00am", true, true, false),
                timelineRow("Agent xxx added remark: \"Hello There\"", "21 Jan 2020 2:00am", false, true, false),
                timelineRow("User Uploaded file (test.doc)", "21 Jan 2020 2:00am", false, false, false),
                timelineLastRow("Order Delivered", "", false),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            CustomButton(
              name: "Download Approved File",
            ),
            SizedBox(
              height: 40,
            )
          ],
        ),
      ),
    );
  }
}

Widget timelineRow(String title, String subTile, bool dash, bool prevDash, bool isStart) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      Expanded(
        flex: 1,
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Visibility(
              visible: isStart||!prevDash,
              child: SizedBox(
                height: 20,
              ),
            ),
            Visibility(
              visible: prevDash && !isStart,
              child: Container(
                width: 1,
                height: prevDash?20:10,
                decoration: new BoxDecoration(
                  color: grey,
                  shape: BoxShape.rectangle,
                ),
                child: Text(""),
              ),
            ),
            Visibility(
              visible: prevDash && !isStart,
              child: SizedBox(
                height: prevDash?0:10,
              ),
            ),
            Container(
              width: 20,
              height: 20,
              padding: EdgeInsets.all(1.5),
              decoration: new BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: grey, width: 1)
              ),
              child: Container(
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  color: purple,
                ),
              ),
            ),
            Visibility(
              visible: !prevDash,
              child: SizedBox(
                height: 20,
              ),
            ),
            Visibility(
              visible: prevDash,
              child: Container(
                width: 1,
                height: dash?20:10,
                decoration: new BoxDecoration(
                  color: grey,
                  shape: BoxShape.rectangle,
                ),
                child: Text(""),
              ),
            ),
            Visibility(
              visible: prevDash,
              child: SizedBox(
                height: dash?0:10,
              ),
            )
          ],
        ),
      ),
      SizedBox(
        width: 10,
      ),
      Expanded(
        flex: 9,
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              title,
                style: TextStyle(
                    fontFamily: "regular",
                    fontSize: 14,
                    color: black)),
            Text(
              subTile,
              style: TextStyle(
                  fontFamily: "regular",
                  fontSize: 10,
                  color: Colors.black54
              ),
            )
          ],
        ),
      ),
    ],
  );
}
Widget timelineLastRow(String title, String subTile, bool prevDash) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Expanded(
        flex: 1,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Visibility(
              visible: !prevDash,
              child: SizedBox(
                height: 20,
              ),
            ),
            Visibility(
              visible: prevDash,
              child: Container(
                width: 1,
                height: prevDash?20:10,
                decoration: new BoxDecoration(
                  color: grey,
                  shape: BoxShape.rectangle,
                ),
                child: Text(""),
              ),
            ),
            Visibility(
              visible: prevDash,
              child: SizedBox(
                height: prevDash?0:10,
              ),
            ),
            Container(
              width: 20,
              height: 20,
              padding: EdgeInsets.all(1.5),
              decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: grey, width: 1)
              ),
              child: Container(
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  color: purple,
                ),
              ),
            ),
            Container(
              width: 3,
              height: 20,
              decoration: new BoxDecoration(
                color: Colors.transparent,
                shape: BoxShape.rectangle,
              ),
              child: Text(""),
            ),
          ],
        ),
      ),
      SizedBox(
        width: 10,
      ),
      Expanded(
        flex: 9,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            DashedContainer(
              height: 40,
              width: Get.width*0.6,
              name: "Please Upload Civil ID here",
              center: true,
            )
          ],
        ),
      ),
    ],
  );
}





class ShowDetails extends StatelessWidget {
  final String cat,ans;
  ShowDetails({this.ans,this.cat});
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 20,
        ),
        Text(
          cat+":",
          textAlign: TextAlign.left,
          style: TextStyle(
              color: grey,
              fontSize: 12,
              fontWeight: FontWeight.w400
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          ans,
          textAlign: TextAlign.left,
          style: TextStyle(
              color: black,
              fontSize: 12,
              fontWeight: FontWeight.w400
          ),
        ),
      ],
    );
  }
}

