import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:squrex/Custom/Drawer.dart';
import 'package:squrex/Utils/Colors.dart';
import 'package:squrex/Utils/Strings.dart';
import 'package:squrex/Utils/TextStyles.dart';

class ImageViewer extends StatelessWidget {
  final String image;
  ImageViewer({this.image});
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: black,
      appBar: AppBar(
        backgroundColor: black,
        elevation: 6,
        shadowColor: grey,
        leading: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: GestureDetector(
            onTap: (){
              Get.back();
            },
            child: Icon(
              Icons.arrow_back,
              size: 25,
              color: white,
            ),
          ),
        ),
        centerTitle: true,
        title: Text(
          photoAppBar,
          style: appBarStyle.copyWith(
            color: white
          ),
        ),
      ),
      drawer: CustomDrawer(),
      body: Container(
        height: Get.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: Get.height*0.4,
              width: Get.width,
              child: Image(
                image: NetworkImage(image),
                fit: BoxFit.fill,
              ),
            )
          ],
        )
      ),
    );
  }
}