import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:squrex/Custom/BarIcon.dart';
import 'package:squrex/Custom/Drawer.dart';
import 'package:squrex/Screens/ImageViewer.dart';
import 'package:squrex/Utils/Colors.dart';
import 'package:squrex/Utils/Strings.dart';
import 'package:squrex/Utils/TextStyles.dart';

List<bool>select = [true,false,false,false];


class PhotoGallery extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final List<String> categories = ["House", "Shop", "Tower", "Corporation"];
  final List<String> imageLinks = ["https://thumbor.forbes.com/thumbor/fit-in/1200x0/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F1026205392%2F0x0.jpg", "https://images.unsplash.com/photo-1570129477492-45c003edd2be?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80", "https://images.unsplash.com/photo-1580587771525-78b9dba3b914?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: white,
      appBar: AppBar(
        backgroundColor: white,
        elevation: 2,
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: GestureDetector(
              onTap: (){
                _scaffoldKey.currentState.openDrawer();
              },
              child: BarIcon(),
            ),
          ),
        ),
        centerTitle: true,
        title: Text(
          photoAppBar,
          style: appBarStyle,
        ),
      ),
      drawer: CustomDrawer(index: 3,),
      body: Container(
        height: Get.height,
        child: Stack(
          children: [
            ListView(
              children: [
                SizedBox(
                  height: Get.height*0.03,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Row(
                      children: categories.map((e){
                        return CategoryCard(name: e,index: categories.indexOf(e),);
                      }).toList(),
                    ),
                  ),
                ),
                SizedBox(
                  height: Get.height*0.03,
                ),
                GridView.count(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  padding: EdgeInsets.all(10),
                  crossAxisCount: 2,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  childAspectRatio: 1.3,
                  children: imageLinks.map((e){
                    return ImageCard(
                      image: e,
                    );
                  }).toList(),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ImageCard extends StatelessWidget {
  final String image;
  ImageCard({this.image});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Get.to(ImageViewer(image: image,));
      },
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10)
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Image(
            fit: BoxFit.fill,
            image: NetworkImage(image),
          ),
        ),
      ),
    );
  }
}



class CategoryCard extends StatelessWidget {
  final String name;
  final int index;
  CategoryCard({this.name, this.index});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){

      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 5),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
          decoration: BoxDecoration(
            gradient: select[index]?LinearGradient(
                colors: [Color(0xffba68c8),Color(0xff7e57c2)],
                begin: AlignmentDirectional.topStart,
                end: AlignmentDirectional.bottomEnd
            ):LinearGradient(
              colors: [white,white]
            ),
            borderRadius: BorderRadius.all(Radius.circular(15)),
            border: Border.all(color: select[index]?Color(0xffba68c8):grey, width: 1)
          ),
          child: Center(
            child: Text(
              name,
              style: TextStyle(
                  color: select[index]?white:grey,
                  fontSize: 14,
                  fontWeight: FontWeight.w300
              ),
            ),
          )
      ),
    );
  }
}
