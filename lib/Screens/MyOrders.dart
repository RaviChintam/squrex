import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:squrex/Custom/BarIcon.dart';
import 'package:squrex/Custom/Drawer.dart';
import 'package:squrex/Screens/OrderDetailPage.dart';
import 'package:squrex/Utils/Colors.dart';
import 'package:squrex/Utils/Strings.dart';
import 'package:squrex/Utils/TextStyles.dart';

class MyOrders extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: white,
      appBar: AppBar(
        backgroundColor: white,
        elevation: 2,
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: GestureDetector(
              onTap: (){
                _scaffoldKey.currentState.openDrawer();
              },
              child: BarIcon(),
            ),
          ),
        ),
        centerTitle: true,
        title: Text(
          orderAppBar,
          style: appBarStyle,
        ),
      ),
      drawer: CustomDrawer(index: 1,),
      body: Container(
        height: Get.height,
        child: Stack(
          children: [
            ListView(
              children: [
                SizedBox(
                  height: Get.height*0.05,
                ),
                OrderDetails(),
                OrderDetails()
              ],
            )
          ],
        ),
      ),
    );
  }
}




class OrderDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Get.to(OrderDetailPage());
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: Get.width*0.08, vertical: 15),
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: white,
            borderRadius: BorderRadius.all(Radius.circular(15)),
            boxShadow: [
              BoxShadow(
                  color: greyShade,
                  offset: Offset(3.0,3.0),
                  blurRadius: 15.0,
                  spreadRadius: 1.0
              ),
              BoxShadow(
                  color: white,
                  offset: Offset(-3.0,-3.0),
                  blurRadius: 15.0,
                  spreadRadius: 1.0
              ),
            ]
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      orderId +"35642362",
                      textAlign: TextAlign.left,
                      style: orderIdStyle
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "21 Jan 2020 2:00am",
                      textAlign: TextAlign.left,
                      style: orderDateStyle,
                    ),

                  ],
                ),
                SizedBox(
                  width: Get.width*0.1,
                ),
                Cat(ques: status, ans: "Order Created",)
              ],
            ),
            SizedBox(
              height: 20,
            ),

            Cat(
              ques: category,
              ans:"Abc"
            ),

            SizedBox(
              height: 20,
            ),

            Cat(
              ques: subCat,
              ans: "Xyz",
            )

          ],
        ),
      ),
    );
  }
}



class Cat extends StatelessWidget {
  final String ques,ans;
  Cat({this.ans,this.ques});
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          ques,
          textAlign: TextAlign.left,
          style: catQuesStyle,
        ),
        SizedBox(
          height: 5,
        ),
        Text(
          ans,
          textAlign: TextAlign.left,
          style: catAnsStyle,
        ),

      ],
    );
  }
}


