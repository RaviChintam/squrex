import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:squrex/Custom/BottomImage.dart';
import 'package:squrex/Custom/CustomButton.dart';
import 'package:squrex/Custom/CustomTextFieldOutline.dart';
import 'package:squrex/Screens/VerificationPage.dart';
import 'package:squrex/Utils/Strings.dart';
import 'package:squrex/Utils/TextFieldDecor.dart';
import 'package:squrex/Utils/TextStyles.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Positioned(
              bottom: 0,
              child: BottomImage()
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height*0.12,
                ),
                Text(
                  appName,
                  style: loginHeaderStyle,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height*0.08,
                ),
                Text(
                  welcome,
                  style: welcomeStyle,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height*0.015,
                ),
                Text(
                  loginMessage,
                  style: loginMessageStyle,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height*0.05,
                ),
                TextFieldOutline(
                  child: TextField(
                    keyboardType: TextInputType.phone,
                    decoration: inputFieldStyle.copyWith(
                      hintText: loginFieldHint
                    )
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height*0.04,
                ),
                GestureDetector(
                  onTap: (){
                    Get.to(VerificationPage(), transition: Transition.rightToLeft, duration: Duration(milliseconds: 400));
                  },
                  child: CustomButton(
                    name: con,
                  )
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height*0.03,
                ),
                Text(
                  loginHint,
                  style: loginHintStyle
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
