import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:squrex/Custom/BarIcon.dart';
import 'package:squrex/Custom/CustomButton.dart';
import 'package:squrex/Custom/Drawer.dart';
import 'package:squrex/Utils/Colors.dart';
import 'package:squrex/Utils/Decorations.dart';
import 'package:squrex/Utils/Strings.dart';
import 'package:squrex/Utils/TextFieldDecor.dart';

class ContactUs extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      key: _scaffoldKey,
      backgroundColor: white,
      appBar: AppBar(
        backgroundColor: white,
        elevation: 2,
        leading: Container(
          margin: EdgeInsets.only(left: 10),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 20),
            child: GestureDetector(
              onTap: (){
                _scaffoldKey.currentState.openDrawer();
              },
              child: BarIcon(),
            ),
          ),
        ),
        centerTitle: true,
        title: Text(
          contactAppBar,
          style: TextStyle(
              color: black,
              fontSize: 16,
              fontWeight: FontWeight.w500
          ),
        ),
      ),
      drawer: CustomDrawer(index: 4,),
      body: Container(
        height: Get.height,
        child: Stack(
          children: [
            ListView(
              children: [
                SizedBox(
                  height: Get.height*0.05,
                ),
                Container(
                  height: 50,
                  margin: EdgeInsets.symmetric(horizontal: Get.width*0.1, vertical: 10),
                  decoration: fieldBoxDecor,
                  child: TextField(
                    textInputAction: TextInputAction.next,
                    decoration: contactFieldDecor.copyWith(
                      hintText: nameContactField
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  margin: EdgeInsets.symmetric(horizontal: Get.width*0.1, vertical: 10),
                  decoration: fieldBoxDecor,
                  child: TextField(
                    textInputAction: TextInputAction.next,
                    decoration: contactFieldDecor.copyWith(
                        hintText: emailField
                    ),
                  ),
                ),
                Container(
                  height: 50,
                  margin: EdgeInsets.symmetric(horizontal: Get.width*0.1, vertical: 10),
                  decoration: fieldBoxDecor,
                  child: TextField(
                    textInputAction: TextInputAction.next,
                    decoration: contactFieldDecor.copyWith(
                        hintText: subjectField
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: Get.width*0.1, vertical: 10),
                  decoration: fieldBoxDecor,
                  child: TextField(
                    maxLines: 6,
                    decoration: contactFieldDecor.copyWith(
                        hintText: yourMessageField,
                        contentPadding: EdgeInsets.symmetric(horizontal: 15, vertical: 10)
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: Get.height*0.05,
              child: Container(
                width: Get.width,
                child: Center(
                  child: GestureDetector(
                    onTap: (){

                    },
                    child: CustomButton(
                      name: contactButton,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}